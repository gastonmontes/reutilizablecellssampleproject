//
//  BaseCellModel.swift
//  ReutilizableCellsSample
//
//  Created by Gaston  Montes on 06/12/2018.
//  Copyright © 2018 Gaston  Montes. All rights reserved.
//

import Foundation

class BaseCellModel: BaseCellModelProtocol {
    // MARK: - BaseCellModelProtocol implementation.
    func cellIdentifier() -> String {
        return ""
    }
}
