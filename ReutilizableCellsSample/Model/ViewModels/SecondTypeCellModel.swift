//
//  SecondTypeCellModel.swift
//  ReutilizableCellsSample
//
//  Created by Gaston  Montes on 06/12/2018.
//  Copyright © 2018 Gaston  Montes. All rights reserved.
//

import Foundation

class SecondTypeCellModel: BaseCellModel {    
    // MARK: - BaseCellModelProtocol implementation.
    override func cellIdentifier() -> String {
        return "SecondTypeCell"
    }
}
