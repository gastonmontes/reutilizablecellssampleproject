//
//  BaseCellProtocol.swift
//  ReutilizableCellsSample
//
//  Created by Gaston  Montes on 06/12/2018.
//  Copyright © 2018 Gaston  Montes. All rights reserved.
//

import Foundation

protocol BaseCellProtocol {
    func cellSetModel(_ model: BaseCellModel)
}
