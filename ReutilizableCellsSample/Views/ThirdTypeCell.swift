//
//  ThirdTypeCell.swift
//  ReutilizableCellsSample
//
//  Created by Gaston  Montes on 06/12/2018.
//  Copyright © 2018 Gaston  Montes. All rights reserved.
//

import UIKit

class ThirdTypeCell: BaseTypeCell {
    // MARK: - BaseCellProtocol implementation.
    override func cellSetModel(_ model: BaseCellModel) {
        self.backgroundColor = UIColor.yellow
    }
}
