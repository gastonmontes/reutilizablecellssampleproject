//
//  SampleCellsViewController.swift
//  ReutilizableCellsSample
//
//  Created by Gaston  Montes on 05/12/2018.
//  Copyright © 2018 Gaston  Montes. All rights reserved.
//

import UIKit

class SampleCellsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // MARK: - IBOutlets vars.
    @IBOutlet private weak var sampleCellsTableView: UITableView!
    
    // MARK: - Vars.
    private var sampleCellsModels = Array<Array<BaseCellModel>>()
    
    // MARK: - View life cycle.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fetchData()
        self.configureTableView()
    }
    
    // MARK: - Data functions.
    private func fetchData() {
        var firstSectionCellModels = Array<BaseCellModel>()
        firstSectionCellModels.append(FourthTypeCellModel())
        firstSectionCellModels.append(FirstTypeCellModel())
        firstSectionCellModels.append(ThirdTypeCellModel())
        firstSectionCellModels.append(SecondTypeCellModel())
        firstSectionCellModels.append(ThirdTypeCellModel())
        firstSectionCellModels.append(SecondTypeCellModel())
        firstSectionCellModels.append(FirstTypeCellModel())
        firstSectionCellModels.append(FourthTypeCellModel())
        
        var secondSectionCellModels = Array<BaseCellModel>()
        secondSectionCellModels.append(SecondTypeCellModel())
        secondSectionCellModels.append(ThirdTypeCellModel())
        secondSectionCellModels.append(FirstTypeCellModel())
        secondSectionCellModels.append(FirstTypeCellModel())
        secondSectionCellModels.append(FourthTypeCellModel())
        
        self.sampleCellsModels.append(firstSectionCellModels)
        self.sampleCellsModels.append(secondSectionCellModels)
    }
    
    // MARK: - UITableViewCell configuration.
    private func configureTableView() {
        for sectionModel in self.sampleCellsModels {
            for cellModel in sectionModel {
                self.sampleCellsTableView.register(UINib(nibName: cellModel.cellIdentifier(), bundle: Bundle.main), forCellReuseIdentifier: cellModel.cellIdentifier())
            }
        }
    }
    
    // MARK: - UITableViewDataSource implementation.
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sampleCellsModels.count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionModels = self.sampleCellsModels[section]
        return sectionModels.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionModels = self.sampleCellsModels[indexPath.section]
        let cellModel = sectionModels[indexPath.row]
        
        let cell = self.sampleCellsTableView.dequeueReusableCell(withIdentifier: cellModel.cellIdentifier(),
                                                                 for: indexPath) as! BaseTypeCell
        cell.cellSetModel(cellModel)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Section number: \(section)"
    }
}
